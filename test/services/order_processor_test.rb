require 'test_helper'

class OrderProcessorTest < ActiveSupport::TestCase

  def setup
    pr_fulfill = ProductRequest.where(can_fulfill: true).first
    pr_unfulfill = ProductRequest.where(can_fulfill: false).first

    @valid_order = Order.new(product_requests: [pr_fulfill])
    @invalid_order = Order.new(product_requests: [pr_unfulfill])
  end

  test 'should build order items for fulfillable order' do
    OrderProcessor.new(@valid_order).call
    assert !@valid_order.order_items.empty?
  end

  test 'should not build order items for unfulfillable order' do
    OrderProcessor.new(@invalid_order).call
    assert @invalid_order.order_items.empty?
  end

  test 'should return min product_packages for valid product_request' do
    OrderProcessor.new(@valid_order).call
    assert @valid_order.order_items.length == 1 && @valid_order.order_items.first.quantity == 2
  end

end
